---
title: 2022 Retrospective
description: The year 2022 was a year of refining the idea and finding a strong and immediately useful application of Anagolay technology. At the beginning of the year, we started with an ambitious goal of bringing a robust and scalable framework for rights management to the world.
authors: [elena, daniel]
tags:
  - retrospective
  - summary post
  - libraries
  - waat
  - macula
  - remote-signer
image: https://files.kelp.digital/kelp-digital-assets/anagolay-network/blog/assets/Anagolay_Blog_Retrospective2022.jpg
date: 2023-02-14
---

![2022 retro banner](https://files.kelp.digital/kelp-digital-assets/anagolay-network/blog/assets/Anagolay_Blog_Retrospective2022.jpg)

What have we done in 2022? In two words, **A LOT!**

## Overall highlights

- we wrote and published [Anagolay’s White Paper](https://bit.ly/Anagolay_WhitePaper_wip)
- created six custom pallets to support Anagolay’s vision
- ran standalone testnet for 1 million blocks and 15 runtime updates without downtime
- migrated runtime source code from standalone node to parachain node
- rebranded 3 websites, including brand-new docs
- published 7 blog posts, and 7 demo videos
- built 4 web apps (svelte), including the chrome extension, aka Anagolay Wallet
- built and released first ever image processing service built on top of IPFS
- to support all the above, we built ~10 important libraries

The year 2022 was a year of refining the idea and finding a strong and immediately useful application of Anagolay technology. At the beginning of the year, we started with an ambitious goal of [bringing a robust and scalable framework for rights management](./project-idiyanale-phase-1/milestone-1.md) to the world.

<!--truncate-->

TLDR;

This didn’t, however, resonate with the community and investors as well as we thought it should. We came to realize that `Rights management` is too broad a topic, and people tend to understand it very differently based on their background. What seemed obvious and immediately needed to us from the technical and legal perspective, turned out to be a challenge to transmit and convey - at the end of the day, hardly anybody understood what we were trying to accomplish🫤

Instead of explaining the future potential at scale, we’ve decided to focus on finding an immediate showcase. Something rather simple - but immediately useful.

Then it hit us, one of the most important features of the current creator's age is being underused and monopolized. That is`tipping`, or some call it `creator support` and `subscription management`. We see a big rise in centralized solutions for both off-platform (Patreon) and on-platform monetization (Twitch, YouTube, Insta, Tiktok, etc.). Generally, only a small percentage of creators can make substantial profits from these. To succeed in the monetization game, you need to juggle multiple platforms & tools and be a full-time community builder on top of doing creative work. Most _original_ creators do not have what it takes - time or sufficient incentive to play these games - so the content they create is either monetized by others or never sees the light of day.

We believe monetizing and supporting creative work should be simpler and equally accessible for everyone. So we decided to build a solution on top of our Rights Management to make this possible. The solution creators can use in addition to the monetization that they get through centralized platforms. It’s not exclusive; it just makes things simpler. Streamlining the process for everyone.

In 2022 we started assembling the pieces to make this vision a reality. We still have a long way to go, and 2023 is looking bright. With each project we build we aim to bring real value to the creative community and help creators earn more with less stress.

This document is split into three sections, each corresponding to a top-level project like **Anagolay**, **Macula**, and **Remote Signer**. Each section contains a description and a list of components we implemented and built.

## Anagolay

[Anagolay Framework](https://anagolay.network/) was our key focus throughout 2022 and took a huge leap from an early concept to a well-defined defined set of ideas and implementations, which are outlined in the [whitepaper](https://bit.ly/Anagolay_WhitePaper_wip) and documented on [anagolay.dev](/).

### Grants and Programs

The team received and (over) delivered two Web3 Foundation Grants totaling 4 milestones, where each milestone was 1 month. [Grant 1](https://github.com/w3f/Grants-Program/pull/719) and [Grant 2](https://github.com/w3f/Grants-Program/pull/1224) links.

We successfully finalized and delivered the Substrate Builders program Milestone 1 ( 3 months ).

### Explorer

The purpose of this app is to enable users to use Anagolay-specific features in a carefully designed and friendly way. All the generic substrate functionalities are not included; they are linked to the great polakdot-js app with the correct network RPC.

Today with the Anagolay app, you can:

- verify your domain and subdomain
- enable tipping for verified domains and subdomains
- see the statistic for each channel ( domain or subdomain ) with the total earned
- build and save a Workflow using the intuitive editor

In 2023 you will be able to:

- verify your
  - Twitter
  - Mastodon ( any federated instance )
  - GitLab
  - GitHub
  - StackOverflow
  - Youtube
  - Twitch
- manage your ownership and copyright Statements
- assign Licenses to your content
- accept payments in more currencies, including fiat
- create Tiers and exclusive deals

### Chrome extension wallet

If there are creators who create and sell stuff, then there must be a way to support creations and pay for the stuff 😊. That is the purpose of the Anagolay Extension. To be a way to pay and support creators, be that on their websites or the usernames on centralized platforms. In 2022 we introduced a lightweight wallet with the extension, but that will change in 2023. We are working on a solution that will help your private keys to be truly private and not kept in the browsers as most wallets have it.

Today with Anagolay Extension, you can:

- see if the creators’ channel ( a website ) is verified or not
- see how they have verified the channel and who is the owner
- send a one-time tip of your choosing in IDI ( our native ) token
- create the Anagolay account and receive tokens
- import existing substrate-based accounts and receive tokens
- feel good after 🙂

To start with the extension, check out the [Intro to Anagolay Wallet tutorial ](./chrome_extension_wallet/installation-and-step-by-step-tutorial.md)

### Runtime Node

The core of our innovation and the engine of next-gen Rights Management.

Needless to say that this is by far the hardest thing to build in a truly decentralized and distributed way. With the help of the excellent substrate framework by Parity and the ingenuity of our developers, we are pushing the limits of known definitions of blockchain.

Our runtime should be:

- interplanetary
- self-governed
- self-sufficient
- energy-efficient

For every pallet and functionality we design and implement, we try to keep to the 4 points from above.

The Pallets available today:

- [operation](../../docs/anagolay/operation/) — A base building block for Workflows on-chain and off-chain processes.
- [workflow](../../docs/anagolay/workflow/) — Connect many operations in a flow to create powerful second-tier processes
- [verification](../../docs/anagolay/verification/) — Verify online resources like domains and usernames, (think Keybase)
- [tipping](../../docs/anagolay/tipping/) — Payments and monetary support for verified online resources
- [proof_of_existence (evidence)](../../docs/anagolay/proof-of-existence/) — List of Items (proofs) that holds a list of identifiers for any digital resource
- [statement](../../docs/anagolay/statement/) — The actual statement of a right for the digital asset

Apart from building new cool tech, we also did plenty on testing our hypothesis and the functionality of what we’ve built.

Here are quick numbers:

- **8** months without the downtime with 15 runtime updates — Standalone testnet
- **1** month took us to migrate the code from standalone node to parachain node
- **2** published Workflows
- **3** published Operations
- **10** verified channels
- **15** runtime upgrades
- **100+** tips sent
- **~2000 IDI** received by the verified creators

## Macula

<aside>
ℹ️ Macula is in PRIVATE BETA and licensed under the SSPL

</aside>

Macula is a group name for many microservices built to support growing web3 demand and popularity of the IPFS. Today we ship webapps faster than ever with much more digital assets. To handle the ever-rising quality of the images, the image processing is done on CI servers or local laptops, then shipped with the app which tends to occupy gigabytes of storage. No matter where you host this you will pay for it, either on AWS, Google or Azure.

We have asked ourselves a question, `Why not use on-the-fly image processing service with IPFS with small chunks so we can have deduplication and minimize the storage size and cost?` Turns out that the solution we needed doesn’t exist… until now.

Today with Macula you can process images on the IPFS and store their renditions on the same IPFS node, effectively turning your IPFS node into the Imgix. You can also host your SPA or SSG website on IPFS with built in brotli and gzip.

### Microservices

- image processing built on the sharp library which is using libvips — faaaaaast
- hosting - like surge.sh but free and open source
  - versioned — this means you can access versions of your website by referencing the CID
  - subdomain based — static subdomain name ALWAYS pointing to latest release
- secure ipfs API gateway using WAAT or API-KEY — like pinata but free, secure and open source
- public read-only IPFS gateway
- public read-only IPFS gateway with Authorization token for viewing restricted content

### Libraries

Suporting libraries for our infra and our products.

#### waat - Web Api Authentication Token

Stateless token built with decentralization in mind. While WAAT and other token schemes are similar in the aspect of the need of the centralized resource to issue token, the major difference is in the definition of `centralized`. In other token schemes, this is actual server hosted and ran by central authority. In WAAT context, the `centralized` is the place that contains the user's **PRIVATE** key. This can be any browser-based wallet like Metamask, PolkadotJS app, Talisman, Subwallet or Remote Signer.

[👩‍💻 Bring me to the code, NOW!](https://github.com/kelp-hq/oss/blob/2df2dae5654ec4640065a280d131a97d529d2439/libraries/web3-api-auth-token/README.md)

#### [rushstack](https://rushstack.io/): heft-library-rig

This rig brings modern settings for your library, web app or microservice. Most enterprises like to work with LTS systems and battle-tested packages, which tends to impact the build process. CommonJS is not a bad practice, but let’s be honest, its time is slowly fading away. ESM is the future.

This rig ships with the two profiles, a default which is the ESM first then CommonJS, and pure ESM.

[👩‍💻 Bring me to the code, NOW!](https://github.com/kelp-hq/oss/tree/2df2dae5654ec4640065a280d131a97d529d2439/libraries/heft-library-rig)

#### [rushstack](https://rushstack.io/): heft-esm-transform-plugin

We all know the power of Typescript, and we love it. Unfortunately, there are times when we don’t like it that much. It’s the time when we need to ship a library that contains the CommonJS and ESM code. There are a few ways to solve this issue; one is to use the bundlers like webpack, rollup, esbuild; the other is to write the code in proper syntax ( add `.js` for imports ) then modify the CJS code.

Using bundlers kind of defeats the purpose of clear and unified repo and writing the `.js` at the end of each import feel like cheating since the included files are actually `.ts`.

This plugin doesn’t use any bundler, it crawls through the generated code and uses the regex to match the local imports and then modifies them by adding the `.js`. Yep, you still write your TS files as you would normally do, without the file extension; good ol’ CJS 😉

[👩‍💻 Bring me to the code, NOW!](https://github.com/kelp-hq/oss/tree/2df2dae5654ec4640065a280d131a97d529d2439/libraries/heft-esm-transform-plugin)

#### [sveltekit](https://kit.svelte.dev/): chrome-extension-adapter

With this adapter you can get your sveltekit static app to run as the chrome extension. You write Typescript and use postcss ( tailwind ), and it will be built according to manifest. Additionally the manifest is generated with proper files and their locations relative to the root.

Anagolay web extension is using this.

[👩‍💻 Bring me to the code, NOW!](https://github.com/kelp-hq/oss/tree/2df2dae5654ec4640065a280d131a97d529d2439/libraries/heft-esm-transform-plugin)

#### [sveltekit](https://kit.svelte.dev): adapter-macula

The adapter that will create the manifest and proper build for hosting with the Macula hosting solution. Think like nextjs or netlify but free and open source.

[👩‍💻 Bring me to the code, NOW!](https://github.com/kelp-hq/oss/tree/2df2dae5654ec4640065a280d131a97d529d2439/libraries/heft-esm-transform-plugin)

#### ipfs-cli

Upload the file or a directory in a secure way to any IPFS node that is secured by `x-api-key` header.

[👩‍💻 Bring me to the code, NOW!](https://github.com/kelp-hq/oss/tree/2df2dae5654ec4640065a280d131a97d529d2439/libraries/heft-esm-transform-plugin)

<aside>
ℹ️ Trivia: For you who don’t know what “Macula” is. Macula is an integral part of your eye, and it is responsible for the central, high-resolution color vision that is possible in good light. [[wiki](https://en.wikipedia.org/wiki/Macula)]

</aside>

## Remote Signer

Started as Daniels's private project and a PoC for remote signing the git commits with the GPG keys. Today this approach is expanded to use substrate-based accounts to sign git commits and, in the future, will be the key to our new protocol that includes the WAAT and secure mobile environments. Outcome: bringing the new kind of web-based wallets and improving handling and storing the private keys.

Atm the code is private, but we are making changes for it to contain the proper license model and will open-source it later this year.

Our team has been using Remote Signer internally since march 2022. This means that 99% of commit signatures in all our repos are signed with the Remote Signer.

Components:

- CLI — acts as a gpg program in gitconfig ( `git config --global gpg.program remote_signer` )
- Websocket — currently [Socket.io](http://Socket.io) based, for communicating between server, mobile app and cli
- server — service that runs in cloud containing the keys
- phone app — native application that holds your private keys and knows how to sign with GPG and Substrate

We still believe the `rights management layer`is of utmost importance and a necessary base feature for all web3 and metaverse platforms and projects. Yet 2022 taught us that we need to focus on the smaller challenges, go step by step, rolling out features that can be launched immediately and start bringing value for digital creators right away.

Anagolay [browser extension](#chrome-extension-wallet), [Macula](#macula), [Remote Signer](#remote-signer) and the rest of the components built in 2022 are all critical pieces of the puzzle, that we’ll keep putting together over the course of 2023.

The outcome?

To set up the platform (and the necessary market infrastructure) to make sharing & monetizing creative works appealing to the majority of original creators. Which ultimately comes down to the two things:

- incentivize creators of original content
- streamlining the workflow for creators and the rest of the market participants

Join our community on [Kelp&&Anagolay HQ on Discord](https://discord.gg/75aseEyyvj) to keep up with our progress. You can also
follow us on [Twitter](https://twitter.com/AnagolayNet) and [Mastodon](https://mastodon.social/@anagolay) or [Matrix](https://matrix.to/#/#anagolay-general:matrix.org).
