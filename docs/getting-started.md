# Getting started

## Installation

How to install the Node in developer mode

```yaml
version: '3'
services:
  node:
    image: registry.gitlab.com/anagolay/anagolay:44f164e3
    volumes:
      - anagolay_data:/data
    ports:
      - 30333:30333
      - 9933:9933
      - 9944:9944
      - 9615:9615
    command: |
      /anagolay
      --dev
      --no-telemetry
      --rpc-methods Unsafe
      --rpc-cors all
      --unsafe-rpc-external

volumes:
  anagolay_data:
```

:::info
WIP, docs are in progress, if you want to contribute or just get involved join our [discord](https://discordapp.com/invite/WHe4EuY) or [matrix](https://matrix.to/#/#anagolay-general:matrix.org) channel.
:::
