---
title: IPFS HTTP client
sidebar_label: API Client
tags:
  - ipfs
  - http
  - api
  - client
  - authentication
  - authorization
---

| Name         | Value                          |
| ------------ | ------------------------------ |
| Repository   | https://github.com/kelp-hq/oss |
| README.md    | https://github.com/kelp-hq/oss |
| SSPL LICENSE | https://github.com/kelp-hq/oss |

Lightweight IPFS http client. It is build on top of `cross-fetch` and works as CJS and ESM.

:::info
WIP, docs are in progress, if you want to contribute or just get involved join our [discord](https://discordapp.com/invite/WHe4EuY) or [matrix](https://matrix.to/#/#anagolay-general:matrix.org) channel.
:::
