---
title: Websocket service
description: Microservice acts as a relay for all socket based communication. One of them is notifying the Publish service to build a Workflow or an Operation
tags:
  - anagolay
  - wasm
  - rust
  - nostd
---

| Name       | Value |
| ---------- | ----- |
| Repository | -     |
