---
title: Proof of Existence
description: Anagolay Proof is the set of Workflow output.
authors: [daniel, adriano]
tags:
  - proof
  - poe
---

| Name       | Value                                                            |
| ---------- | ---------------------------------------------------------------- |
| Repository | https://github.com/anagolay/anagolay-chain/tree/main/pallets/poe |
| Pallet     | Yes                                                              |

# Proof of Existence

:::tip

The proof is sufficient evidence or a sufficient argument for the truth of a proposition.

:::

## Overview

Is a structured final output of the Workflow. Each proof is different in a few aspects and similar in others.

Proof params is the collection of the operation outputs mapped via operation name. A `proofId` will always be different for different data, but not the proof params. That's because the proof params are controlled completely by the rule definition and its operations.

## Invalidations / Replacing / Updates

We can invalidate the proof by generating a new one and setting the `prev` field to the previous `proofId` we want to invalidate. In order to get the last proof which is also the valid one we must look for the proof that is not linked to any other proof, in other words where `current.proofId` does not exist in the `anyProof.prev` field.

Generic structure:

```ts
// Generic Proof
interface Proof {
  id: GenericId;
  data: {
    workflowId: GenericId;
    prev: GenericId;
    creator: CreatorId;
    forWhat: ForWhat;
    params: string[];
    context: VerificationContext;
  };
}
```

## Integration with verification pallet

The verification pallet is intended to be used by the substrate community; however, the statements and poe pallets are modified to integrate these previous verification steps and to allow the holder of a successful verification request to produce a Statement of ownership out of the verification result.

At the moment the verification key is produced, a `PoeVerificationKeyGenerator` is called. This is loosely coupled with the proofs pallet to allow the creation of proofs associated to the `VerificationContext`.

Infact the proof contains a `VerificationContext` that tells how the proof was generated; in case it was generated off-chain and no context is verifiable, the `context` field contains the value `Unbounded`.

**Proof generation**

`PoeVerificationKeyGenerator` is implementing verification pallet `VerificationKeyGenerator` and uses an Anagolay workflow to produce a CID out of the identifier given as input. Then, it stores a Proof containing the Verification key in `ProofByProofIdAndAccountId` and `ProofIdsByVerificationContext`.

`ProofIdsByVerificationContext` storage indexes `Vec<ProofId>` values by `VerificationContext` keys: this can be performantly queried. The values are collections coherently with the fact that an Anagolay Workflow can generate multiple proofs for a subject.
